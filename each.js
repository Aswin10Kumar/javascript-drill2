function each(array, callback){
     const newArray=[];
    if(typeof callback !== 'function'){throw new TypeError('Callback parameter is not a function');}
    for(let index in array){
         const checkValue=callback(array[index],index,array);
         if(checkValue != null){
          newArray.push(checkValue);
         }
     }
     return newArray;
}

module.exports= each;

