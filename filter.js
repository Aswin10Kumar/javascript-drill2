const each=require('./each.js');

function arrayFilter(items,callback){ 
    const newArray=(each(items,callback));
    return newArray;
}

module.exports=arrayFilter;