// function reduce(elements, cb, startingValue) {
//     // Do NOT use .reduce to complete this function.
//     // How reduce works: A reduce function combines all elements into a single value going
// from left to right.
//     // Elements will be passed one by one into `cb` along with the `startingValue`.
//     // `startingValue` should be the first argument passed to `cb` and the array element 
//should be the second argument.
//     // `startingValue` is the starting value.  If `startingValue` is undefined then make 
//`elements[0]` the initial value.
// }
const reduce=require('../reduce.js');
const elements = [1,0,2,1,3,2,4,3,5,4,5,5];
var startingValue=0;

function cb(tot,val,index,array){
    return tot + (val*val*val);
}

console.log(reduce(elements,cb,startingValue));