// // use this to test 'flatten'

// function flatten(elements) {
//     // Flattens a nested array (the nesting can be to any depth).
//     // Hint: You can solve this using recursion.
//     // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
// }
const nestedArray = [1, [2,5], [[3]], [[[4]]]]; 
const problem=require('../flatten.js');

console.log(problem.arrayFlatten(nestedArray));


