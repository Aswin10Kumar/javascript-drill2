// function map(elements, cb) {
//     // Do NOT use .map, to complete this function.
//     // How map works: Map calls a provided callback function once for each element in an array, 
//     // in order, and functionructs a new array from the res .
//     // Produces a new array of values by mapping each value in list through a transformation 
//     // function (iteratee).
//     // Return the new array.
// }

const map=require('../map.js');
const elements = [1, 2, 3, 4, 5];

const cb=(value,index,items)=>{
    
    return value*index;
}


console.log(map(elements,cb));
