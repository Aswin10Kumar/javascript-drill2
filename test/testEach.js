// function each(elements, cb) {
//     // Do NOT use forEach to complete this function.
//     // Iterates over a list of elements, yielding each in turn to the `cb` function.
//     // This only needs to work with arrays.
//     // You should also pass the index into `cb` as the second argument
//     // based off http://underscorejs.org/#each
// }

const each=require('../each.js');
const items = [1, 2, 3, 4, 5];
const forEach=(value,index,array)=>{
    //User code 
    console.log(index,value);
}
each(items,forEach);




