function arrayMap(array,callback){ 
    const newArray=[];
    if(typeof callback !== 'function'){throw new TypeError('Callback parameter is not a function');}
    for(let index=0; index<array.length;index++){
        newArray.push(callback(array[index],index,array));
    }
    return newArray;
}
module.exports=arrayMap;