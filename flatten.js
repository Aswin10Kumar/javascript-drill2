const newArray=[];
function arrayFlatten(items){
   
    for(let elements of items){ 
        flatten(elements);
    }
    return newArray;
}

function flatten(elements){
    if(typeof elements == 'number' || typeof elements == String){
        newArray.push(elements);
        return elements;   
    }
    else{
        for(let index in elements){
           flatten(elements[index]);
        }  
    }
   
    return elements;
}

module.exports={arrayFlatten};