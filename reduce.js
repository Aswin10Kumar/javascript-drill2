function reduce(elements,callback,startingValue = 0){
   if(typeof callback !== 'function'){throw new TypeError('Callback parameter is not a function');}
   let start =0;
        if(!startingValue){
            startingValue = elements[0];
            start =1;
        }
        value = startingValue;
        for(let i=start;i< elements.length;i++){
            value = callback(value,elements[i],i,elements);
        }
   return value;
}

module.exports=reduce;